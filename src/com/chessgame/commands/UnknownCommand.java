package com.chessgame.commands;

/**
 * Unknown command
 * 
 * @author dmitriyshilov
 *
 */
public class UnknownCommand implements Command {
}