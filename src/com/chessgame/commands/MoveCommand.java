package com.chessgame.commands;

import com.chessgame.board.Space;

/**
 * Move a piece from firs space to second space
 * 
 * @author dmitriyshilov
 *
 */
public class MoveCommand implements Command {

	private final Space from;

	private final Space to;

	public MoveCommand(final Space from, final Space to) {
		this.from = from;
		this.to = to;
	}

	public Space getFrom() {
		return from;
	}

	public Space getTo() {
		return to;
	}
}