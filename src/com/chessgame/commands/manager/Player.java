package com.chessgame.commands.manager;

import com.chessgame.commands.Command;

/**
 * Player of game
 * 
 * @author dmitriyshilov
 *
 */
public class Player implements CommandManager {
	private final CommandManager commandManager;

	public Player(final CommandManager commandManager) {
		this.commandManager = commandManager;
	}

	@Override
	public Command nextCommand() {
		return commandManager.nextCommand();
	}
}