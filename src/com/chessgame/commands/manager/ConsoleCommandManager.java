package com.chessgame.commands.manager;

import java.io.PrintStream;
import java.util.Scanner;

import com.chessgame.board.Board;
import com.chessgame.board.Space;
import com.chessgame.commands.Command;
import com.chessgame.commands.ExitCommand;
import com.chessgame.commands.MoveCommand;
import com.chessgame.commands.UnknownCommand;

/**
 * Console command producer
 * 
 * @author dmitriyshilov
 *
 */
public class ConsoleCommandManager implements CommandManager {

	private final Board board;

	private final Scanner in = new Scanner(System.in);

	private final PrintStream out = System.out;

	public ConsoleCommandManager(final Board board) {
		this.board = board;
	}

	@Override
	public Command nextCommand() {
		out.println("move a2 a3	- to move a piece from a2 to a3");
		out.println("exit		- exit");
		out.print("command > ");

		final String next = in.nextLine();
		if (next.startsWith("exit")) {
			return new ExitCommand();
		} else if (next.startsWith("move")) {
			final String[] splitted = next.split(" ");

			if (splitted.length < 3) {
				return new UnknownCommand();
			}

			final String from = splitted[1];
			final String to = splitted[2];

			if (from.length() < 2 || to.length() < 2) {
				return new UnknownCommand();
			}

			Space spaceFrom = new Space(from.charAt(0) - 'a', from.charAt(1) - '1');
			Space spaceTo = new Space(to.charAt(0) - 'a', to.charAt(1) - '1');

			if (board.isRotated()) { // need to compensate board rotation
				spaceFrom = new Space(7 - spaceFrom.getI(), 7 - spaceFrom.getJ());
				spaceTo = new Space(7 - spaceTo.getI(), 7 - spaceTo.getJ());
			}
			return new MoveCommand(spaceFrom, spaceTo);
		} else {
			return new UnknownCommand();
		}
	}
}