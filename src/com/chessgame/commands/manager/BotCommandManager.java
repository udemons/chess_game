package com.chessgame.commands.manager;

import java.util.Optional;

import com.chessgame.board.Board;
import com.chessgame.board.Space;
import com.chessgame.commands.Command;
import com.chessgame.commands.ExitCommand;
import com.chessgame.commands.MoveCommand;
import com.chessgame.piece.Piece;
import com.chessgame.piece.PieceColor;
import com.chessgame.piece.rules.RulesProcessor;

/**
 * Command producer of bot
 * 
 * @author dmitriyshilov
 *
 */
public class BotCommandManager implements CommandManager {

	private final Board board;

	private final PieceColor color;

	private Space from;

	boolean isProcessed = false;

	private Space to;

	public BotCommandManager(final Board board, final PieceColor color) {
		this.board = board;
		this.color = color;
	}

	private int depthSearch(final int step) {
		if (step >= 3) {
			return 0;
		}
		final int[] max = { 0 };
		for (int j = 0; j < 8; j++) {
			for (int i = 0; i < 8; i++) {
				final Space from = new Space(i, j);
				if (board.getPiece(from).isPresent()
						&& (board.getPiece(from).get().getColor() == color && board.isRotated()
								|| board.getPiece(from).get().getColor() != color && !board.isRotated())) {
					RulesProcessor.process(board, from, (to) -> {
						max[0] = process(step, max[0], from, to);
					});
				}
			}
		}
		return max[0];
	}

	@Override
	public Command nextCommand() {
		depthSearch(0);
		if (!isProcessed) {
			return new ExitCommand();
		}
		return new MoveCommand(from, to);
	}

	private int process(final int step, int max, final Space from, final Space to) {
		final Optional<Piece> piece = board.move(from, to);
		board.rotate();
		int value = depthSearch(step + 1);
		board.rotate();
		board.move(to, from);

		if (piece.isPresent()) {
			board.add(to, piece.get());
			if (piece.get().getColor() != color) {
				value++;
			}
		}

		if (value >= max) {
			max = value;
			if (step == 0) {
				this.from = from;
				this.to = to;
				isProcessed = true;
			}
		}
		return max;
	}
}
