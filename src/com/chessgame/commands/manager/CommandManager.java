package com.chessgame.commands.manager;

import com.chessgame.commands.Command;

/**
 * Command producer interface
 * 
 * @author dmitriyshilov
 *
 */
public interface CommandManager {

	Command nextCommand();

}
