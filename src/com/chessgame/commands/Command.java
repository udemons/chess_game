package com.chessgame.commands;

/**
 * Some command to do some action on a chess board or in this program
 * 
 * @author dmitriyshilov
 *
 */
public interface Command {
}