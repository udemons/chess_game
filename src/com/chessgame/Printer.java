package com.chessgame;

import java.io.PrintStream;

import com.chessgame.board.Board;
import com.chessgame.board.Space;

/**
 * Board publisher in the console
 * 
 * @author dmitriyshilov
 *
 */
public class Printer {

	private static final PrintStream OUT = System.out;

	private static String getIndex(final boolean isRotated, final int i) {
		if (isRotated) {
			return "" + (char) ('h' - i);
		} else {
			return "" + (char) ('a' + i);
		}
	}

	private static String getLiteralIndex(final boolean isRotated, final int j) {
		if (isRotated) {
			return "" + (8 - j);
		} else {
			return "" + (j + 1);
		}
	}

	public static void print(final Board board) {
		OUT.println();

		printTitle(board);
		for (int j = 7; j >= 0; j--) {
			printFreeLine(board, j);
			printContextLine(board, j);
			printFreeLine(board, j);
		}
		printTitle(board);

		OUT.println();
	}

	private static void printContextLine(final Board board, final int j) {
		OUT.print(getLiteralIndex(board.isRotated(), j) + " | ");
		for (int i = 0; i < 8; i++) {
			final Space space = new Space(i, j);
			if (board.getPiece(space).isPresent()) {
				final String pieceString = board.getPiece(space).get().getColor().getId()
						+ board.getPiece(space).get().getType().getId();
				if (board.isWhiteSpace(space)) {
					OUT.print("  " + pieceString + "  ");
				} else {
					OUT.print("- " + pieceString + " -");
				}
			} else {
				if (board.isWhiteSpace(space)) {
					OUT.print("      ");
				} else {
					OUT.print("------");
				}
			}
		}
		OUT.print(" | " + getLiteralIndex(board.isRotated(), j));
		OUT.println();
	}

	private static void printFreeLine(final Board board, final int j) {
		OUT.print("  | ");
		for (int i = 0; i < 8; i++) {
			if (board.isWhiteSpace(new Space(i, j))) {
				OUT.print("      ");
			} else {
				OUT.print("------");
			}
		}
		OUT.print(" |");
		OUT.println();
	}

	private static void printTitle(final Board board) {
		OUT.print("  ");
		for (int i = 0; i < 8; i++) {
			OUT.print("    " + getIndex(board.isRotated(), i) + " ");
		}
		OUT.println();
	}
}