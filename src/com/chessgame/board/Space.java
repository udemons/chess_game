package com.chessgame.board;

/**
 * Space of chess board
 * 
 * @author dmitriyshilov
 *
 */
public class Space {

	private final int i;

	private final int j;

	public Space(final int i, final int j) {
		this.i = i;
		this.j = j;
	}

	public int getI() {
		return i;
	}

	public int getJ() {
		return j;
	}

	public boolean isCorrect() {
		return 0 <= i && i < 8 && 0 <= j && j < 8;
	}
}
