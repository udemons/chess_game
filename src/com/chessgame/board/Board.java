package com.chessgame.board;

import java.util.Optional;

import com.chessgame.commands.MoveCommand;
import com.chessgame.piece.Piece;
import com.chessgame.piece.PieceColor;
import com.chessgame.piece.PieceType;
import com.chessgame.piece.rules.RulesProcessor;

/**
 * Chess Board
 * 
 * @author dmitriyshilov
 *
 */
public class Board {

	private final Piece[][] board = new Piece[8][8];

	private boolean isRotated = false;

	public Board() {
	}

	public Board(final Board board) {
		this.isRotated = board.isRotated;
	}

	public void add(final Space space, final Piece piece) {
		if (isRotated) {
			board[7 - space.getI()][7 - space.getJ()] = piece;
		} else {
			board[space.getI()][space.getJ()] = piece;
		}
	}

	public void defaultInit() {
		rotate();
		intiPlayer(PieceColor.BLACK);
		rotate();
		intiPlayer(PieceColor.WHITE);
	}

	public Optional<Piece> getPiece(final Space space) {
		if (isRotated) {
			return Optional.ofNullable(board[7 - space.getI()][7 - space.getJ()]);
		} else {
			return Optional.ofNullable(board[space.getI()][space.getJ()]);
		}
	}

	public void intiPlayer(final PieceColor color) {
		for (int i = 0; i < 8; i++) {
			add(new Space(i, 1), new Piece(PieceType.PAWN, color));
		}
		add(new Space(0, 0), new Piece(PieceType.ROCK, color));
		add(new Space(7, 0), new Piece(PieceType.ROCK, color));
		add(new Space(1, 0), new Piece(PieceType.KNIGHT, color));
		add(new Space(6, 0), new Piece(PieceType.KNIGHT, color));
		add(new Space(2, 0), new Piece(PieceType.BISHOP, color));
		add(new Space(5, 0), new Piece(PieceType.BISHOP, color));
		if (color == PieceColor.WHITE) {
			add(new Space(3, 0), new Piece(PieceType.QUEEN, color));
			add(new Space(4, 0), new Piece(PieceType.KING, color));
		} else {
			add(new Space(4, 0), new Piece(PieceType.QUEEN, color));
			add(new Space(3, 0), new Piece(PieceType.KING, color));
		}

	}

	public boolean isPossible(final MoveCommand command) {
		return isPossible(command.getFrom(), command.getTo());
	}

	public boolean isPossible(final Space from, final Space to) {
		if (!from.isCorrect() || !to.isCorrect()) {
			return false;
		}
		if (getPiece(from) == null) {
			return false;
		}
		final boolean[] result = { false };
		RulesProcessor.process(this, from, (to2) -> {
			if (to.getI() == to2.getI() && to.getJ() == to2.getJ()) {
				result[0] = true;
			}
		});
		return result[0];
	}

	public boolean isRotated() {
		return isRotated;
	}

	public boolean isWhiteSpace(final Space space) {
		return (space.getI() + space.getJ()) % 2 == 1;
	}

	public Optional<Piece> move(final MoveCommand command) {
		return move(command.getFrom(), command.getTo());
	}

	public Optional<Piece> move(final Space from, final Space to) {
		if (isRotated) {
			final Piece result = board[7 - to.getI()][7 - to.getJ()];
			board[7 - to.getI()][7 - to.getJ()] = board[7 - from.getI()][7 - from.getJ()];
			board[7 - from.getI()][7 - from.getJ()] = null;
			return Optional.ofNullable(result);
		} else {
			final Piece result = board[to.getI()][to.getJ()];
			board[to.getI()][to.getJ()] = board[from.getI()][from.getJ()];
			board[from.getI()][from.getJ()] = null;
			return Optional.ofNullable(result);
		}

	}

	public void rotate() {
		isRotated = !isRotated;
	}
}