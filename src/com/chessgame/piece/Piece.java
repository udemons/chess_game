package com.chessgame.piece;

/**
 * 
 * @author dmitriyshilov
 *
 */
public class Piece {

	private final PieceColor color;

	private final PieceType type;

	public Piece(final PieceType type, final PieceColor color) {
		this.type = type;
		this.color = color;
	}

	public PieceColor getColor() {
		return color;
	}

	public PieceType getType() {
		return type;
	}
}
