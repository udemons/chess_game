package com.chessgame.piece.rules;

import java.util.function.Consumer;

import com.chessgame.board.Board;
import com.chessgame.board.Space;
import com.chessgame.piece.PieceType;

/**
 * Rules processor need to control different actions
 * 
 * @author dmitriyshilov
 *
 */
public class RulesProcessor {

	public static void process(final Board board, final Space from, final Consumer<Space> consumer) {
		for (final Rule rule : board.getPiece(from).get().getType().getRules()) {
			for (int s = 1; s <= rule.getMaxSteps(); s++) {
				final Space to = new Space(from.getI() + rule.getDi() * s, from.getJ() + rule.getDj() * s);
				if (to.isCorrect() && (rule.isMove() && !board.getPiece(to).isPresent()
						|| rule.isTake() && board.getPiece(to).isPresent())) {
					consumer.accept(to);
				}
				if (to.isCorrect() && board.getPiece(to).isPresent()) {
					break;
				}
			}
		}
		// we weren't able to implement in rules, because here we need use other states
		if (board.getPiece(from).get().getType() == PieceType.PAWN) {
			final Space to = new Space(from.getI(), from.getJ() + 2);
			if (to.isCorrect() && from.getJ() == 1 && !board.getPiece(to).isPresent()
					&& !board.getPiece(new Space(from.getI(), from.getJ() + 1)).isPresent()) {
				consumer.accept(to);
			}
		}
	}
}