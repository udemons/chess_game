package com.chessgame.piece.rules;

/**
 * 
 * @author dmitriyshilov
 *
 */
public class Rule {

	private final int di;

	private final int dj;

	private final boolean isMove;

	private final boolean isTake;

	private final int maxSteps;

	public Rule(final int di, final int dj) {
		this(di, dj, 1, true, true);
	}

	public Rule(final int di, final int dj, final int maxSteps) {
		this(di, dj, maxSteps, true, true);
	}

	public Rule(final int di, final int dj, final int maxSteps, final boolean isMove, final boolean isTake) {
		this.di = di;
		this.dj = dj;
		this.maxSteps = maxSteps;
		this.isMove = isMove;
		this.isTake = isTake;
	}

	public int getDi() {
		return di;
	}

	public int getDj() {
		return dj;
	}

	public int getMaxSteps() {
		return maxSteps;
	}

	public boolean isMove() {
		return isMove;
	}

	public boolean isTake() {
		return isTake;
	}
}
