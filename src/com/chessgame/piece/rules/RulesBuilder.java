package com.chessgame.piece.rules;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dmitriyshilov
 *
 */
public class RulesBuilder {

	private final List<Rule> rules = new ArrayList<>();

	public RulesBuilder add(final Rule rule) {
		rules.add(rule);
		return this;
	}

	public List<Rule> build() {
		return rules;
	}
}
