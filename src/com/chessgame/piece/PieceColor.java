package com.chessgame.piece;

/**
 * 
 * @author dmitriyshilov
 *
 */
public enum PieceColor {

	BLACK("B", "Black"), WHITE("W", "White");

	final String fullName;

	final String id;

	PieceColor(final String id, final String fullName) {
		this.id = id;
		this.fullName = fullName;
	}

	public String getFullName() {
		return fullName;
	}

	public String getId() {
		return id;
	}
}
