package com.chessgame.piece;

import java.util.List;

import com.chessgame.piece.rules.Rule;
import com.chessgame.piece.rules.RulesBuilder;

/**
 * 
 * @author dmitriyshilov
 *
 */
public enum PieceType {

	BISHOP("B", "Bishop", new RulesBuilder().add(new Rule(-1, -1, 7)).add(new Rule(-1, 1, 7)).add(new Rule(1, -1, 7))
			.add(new Rule(1, 1, 7)).build()),
	//@formatter:off
	KING("K", "King", 
			new RulesBuilder()
				.add(new Rule(-1, -1))
				.add(new Rule(-1, 0))
				.add(new Rule(-1, 1))
				.add(new Rule(0, 1))
				.add(new Rule(1, 1))
				.add(new Rule(1, 0))
				.add(new Rule(1, -1))
				.add(new Rule(0, -1))
				.build()
			),
	KNIGHT("k", "Knight",
			new RulesBuilder()
				.add(new Rule(-1, -2))
				.add(new Rule(-2, -1))
				.add(new Rule(-1, 2))
				.add(new Rule(2, -1))
				.add(new Rule(1, 2))
				.add(new Rule(2, 1))
				.add(new Rule(1, -2))
				.add(new Rule(-2, 1))
				.build()
			),
	PAWN("P", "Pawn",
			new RulesBuilder()
				.add(new Rule(0, 1, 1, true, false))
				.add(new Rule(1, 1, 1, false, true))
				.add(new Rule(-1, 1, 1, false, true))
				.build()
		),
	QUEEN("Q", "Queen",
			new RulesBuilder()
				.add(new Rule(-1, -1, 7))
				.add(new Rule(-1, 0, 7))
				.add(new Rule(-1, 1, 7))
				.add(new Rule(0, 1, 7))
				.add(new Rule(1, 1, 7))
				.add(new Rule(1, 0, 7))
				.add(new Rule(1, -1, 7))
				.add(new Rule(0, -1, 7))
				.build()
			),
	ROCK("R", "Rock",
			new RulesBuilder()
				.add(new Rule(-1, 0, 7))
				.add(new Rule(1, 0, 7))
				.add(new Rule(0, 1, 7))
				.add(new Rule(0, -1, 7))
				.build()
		);
	//@formatter:on

	final String fullName;

	final String id;

	final List<Rule> rules;

	PieceType(final String id, final String fullName, final List<Rule> rules) {
		this.id = id;
		this.fullName = fullName;
		this.rules = rules;
	}

	public String getFullName() {
		return fullName;
	}

	public String getId() {
		return id;
	}

	public List<Rule> getRules() {
		return rules;
	}
}