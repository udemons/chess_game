package com.chessgame;

import com.chessgame.board.Board;
import com.chessgame.commands.Command;
import com.chessgame.commands.ExitCommand;
import com.chessgame.commands.MoveCommand;
import com.chessgame.commands.manager.BotCommandManager;
import com.chessgame.commands.manager.ConsoleCommandManager;
import com.chessgame.commands.manager.Player;
import com.chessgame.piece.PieceColor;

/**
 * Main class of this project
 * 
 * @author dmitriyshilov
 *
 */
public class ChessGameMain {

	void solve() {
		final Board board = new Board();
		board.defaultInit();

		final Player player1 = new Player(new ConsoleCommandManager(board));
		final Player player2 = new Player(new BotCommandManager(board, PieceColor.BLACK)); // second player is bot

		boolean isFirstPlayer = true;
		while (true) {
			Printer.print(board);
			Command command;
			if (isFirstPlayer) {
				command = player1.nextCommand();
			} else {
				command = player2.nextCommand();
			}
			if (command instanceof ExitCommand) {
				break;
			}
			if (command instanceof MoveCommand && board.isPossible((MoveCommand) command)) {
				board.move((MoveCommand) command);
				board.rotate();
				isFirstPlayer = !isFirstPlayer;
			}
		}
	}

	public static void main(final String[] args) {
		System.out.println("Welcome to the chess game!");
		new ChessGameMain().solve();
		System.out.println("Good buy!");
	}
}
